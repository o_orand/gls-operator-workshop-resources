# myapp/__init__.py

# Imports
from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

# initalize app
myapp = Flask(__name__)

myapp.config.from_object(Config)
mydb = SQLAlchemy(myapp)
migrate = Migrate(myapp, mydb)

# import app modules
from .views import main_index
from .models import *
